/**
 * SET_HOUR is  STATE for setting hour for alarm.
 * @author nattapat sukpootanan. 
 * */
public class SET_HOUR extends ClockState{
	/**Constructor*/
	public SET_HOUR(Clock clock){
		super(clock);
	}
	/**set STATE to SET_MIN*/
	@Override
	public void performSet() {
		clock.setState(clock.SET_MIN);
		
	}
	/**updating time to display*/
	@Override
	public void updateTime() {
		super.blinkHour = true;
		super.blinkMin = false;
		super.blinkSec = false;
		
		clock.showHour = clock.AlarmHour;
		clock.showMin = clock.AlarmMin;
		clock.showSec = clock.AlarmSec;
		

	}
	/**add AlarmHour */
	public void performPlus(){
		clock.AlarmHour++;
		if(clock.AlarmHour>23){
			clock.AlarmHour = 0;
		}
	}
	/**minus AlarmHour */
	public void performeMinus(){
		clock.AlarmHour--;
		if(clock.AlarmHour<0){
			clock.AlarmHour = 23;
		}
	}
}
