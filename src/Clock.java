import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
/**
 * Clock get time from operation system.
 * @author nattapat sukpootanan
 * */
public class Clock  extends java.util.Observable{
	protected int hours;
	protected int minutes;
	protected int seconds;
	protected int AlarmHour;
	protected int AlarmMin;
	protected int AlarmSec;
	protected int showHour;
	protected int showMin;
	protected int showSec;
	protected ClockState STATE;
	protected ClockState DISPLAY_TIME;
	protected ClockState SET_ALARM_STATE;
	protected ClockState SET_HOUR;
	protected ClockState SET_MIN;
	protected ClockState SET_SEC;
	protected ClockState ALARM_STATE;
	protected ClockState DISPLAY_ALARM;
	protected boolean isAlarmOn = false;
	protected static Clip clip;
	protected  static AudioInputStream  audio;
	/**Constructor of Clock.*/
	public Clock(){
		
		try {
			clip = AudioSystem.getClip();
		} catch (LineUnavailableException e1) {
//			e1.printStackTrace();
		}
		Class loader = this.getClass();
		URL sound = loader.getResource( "sounds/alarm.wav" );
		try {
			audio = AudioSystem.getAudioInputStream( sound );
		} catch (UnsupportedAudioFileException | IOException e) {
//			e.printStackTrace();
		}
		
		
		
		DISPLAY_ALARM = new DISPLAY_ALARM(this);
		DISPLAY_TIME = new DISPLAY_TIME(this);
		SET_HOUR = new SET_HOUR(this);
		SET_MIN = new SET_MIN(this);
		SET_SEC = new SET_SEC(this);
		ALARM_STATE = new ALARM_STATE(this);

		STATE = DISPLAY_TIME;
		
		
		
	}
	/**
	 * Setting state of Clock.
	 * */
	public void setState(ClockState state){
		this.STATE = state;
	}
	/**
	 * updating the time.
	 * */
	public void updateTime(){
		this.STATE.updateTime();
		super.setChanged();
		super.notifyObservers(this);
	}
	/**
	 * handle set key of each state.
	 * */
	public void handleSetKey() { 
		STATE.performSet(); 
	}
	/**
	 * handle plus key of each state.
	 * */
	public void handlePlusKey(){
		STATE.performPlus();
	}
	/**
	 * handle minus key of each state.
	 * */
	public void handleMinusKey(){
		STATE.performeMinus();
	}
}
