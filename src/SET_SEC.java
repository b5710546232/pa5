/**
 * SET_SEC is STATE for setting seconds of Alarm.
 * @author nattapat sukpootanan.
 * */
public class SET_SEC extends ClockState{
	/**Constructor*/
	public SET_SEC(Clock clock){
		super(clock);
	}
	/**Set STATE to DISPLAY_TIME*/
	@Override
	public void performSet() {
		clock.isAlarmOn = true;
		clock.setState(clock.DISPLAY_TIME);
		
	}
	/**updating Time to display.*/
	@Override
	public void updateTime() {
		super.blinkHour = false;
		super.blinkMin = false;
		super.blinkSec = true;
		
		clock.showHour = clock.AlarmHour;
		clock.showMin = clock.AlarmMin;
		clock.showSec = clock.AlarmSec;
		
	}
	/**add seconds of alarm*/
	public void performPlus(){
		clock.AlarmSec++;
		if(clock.AlarmSec>59){
			clock.AlarmSec = 0;
		}
	}
	/**minus seconds of alarm*/
	public void performeMinus(){
		clock.AlarmSec--;
		if(clock.AlarmSec<0){
			clock.AlarmSec = 59;
		}
	}
}
