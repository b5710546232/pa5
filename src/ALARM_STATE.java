/**
 * ALRAM_STATE is state  when clock is alarming.
 * @author nattapat sukpootanan.
 * */
class ALARM_STATE extends ClockState{

	/**Constructor*/
	public ALARM_STATE(Clock clock)  {
		super(clock);
	}
	/**
	 * performSetButton to DISPLAY_TIME STATE.
	 * */
	@Override
	public void performSet() {
		clock.clip.stop();
		clock.isAlarmOn = false;
		clock.setState(clock.DISPLAY_TIME);
		
	}
	/**
	 * performPlusButton to to DISPLAY_TIME STATE.
	 * */
	public void performPlus(){
		clock.clip.stop();
		clock.isAlarmOn = false;
		clock.setState(clock.DISPLAY_TIME);
	}
	/**
	 * performMinusButton to to DISPLAY_TIME STATE.
	 * */
	public void performMinus(){
		clock.clip.stop();
		clock.isAlarmOn = false;
		clock.setState(clock.DISPLAY_TIME);
	}
	/**
	 * updateTime to display.
	 * */
	@Override
	public void updateTime() {
		super.blinkHour = true;
		super.blinkMin = true;
		super.blinkSec = true;
		
		clock.showHour = clock.AlarmHour;
		clock.showMin = clock.AlarmMin;
		clock.showSec = clock.AlarmSec;
		
	}
	

}
