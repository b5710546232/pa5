import javax.sound.sampled.Clip;
/**
 * DISPLAY_ALARM is STATE for showing alarm time.
 * @author nattapat sukpootanan
 * */

public class DISPLAY_ALARM extends ClockState{
	/**Constructor of DISPLAY_ALARM STATE*/
	public DISPLAY_ALARM(Clock clock){
		super(clock);
	}
	/**updating time to display*/
	public void updateTime(){
		
		super.blinkHour = false;
		super.blinkMin = false;
		super.blinkSec = false;
		
		clock.showHour = clock.AlarmHour;
		clock.showMin = clock.AlarmMin;
		clock.showSec = clock.AlarmSec;
		
		if(clock.AlarmHour==clock.hours&&clock.AlarmMin==clock.minutes&&
				clock.AlarmSec==clock.seconds&&clock.isAlarmOn){		
			Clock.clip.start();
			Clock.clip.loop(Clip.LOOP_CONTINUOUSLY);
			clock.isAlarmOn = false;
			clock.setState( clock.ALARM_STATE);
			
		}
	}
	/**
	 *setting state to DISPLAY_TIME . 
	 * */
	public void performPlus(){
		clock.setState( clock.DISPLAY_TIME);
		
	}


}
