/**
 * SET_MIN is STATE for setting alarm minutes.
 * @author nattapat sukpootanan.
 * */
public class SET_MIN extends ClockState{
	public SET_MIN(Clock clock){
		super(clock);
	}
	/**Set STATE to SET_SEC*/
	@Override
	public void performSet() {
		clock.setState(clock.SET_SEC);
		
	}
	/**updating time to display*/
	@Override
	public void updateTime() {
		super.blinkHour = false;
		super.blinkMin = true;
		super.blinkSec = false;
		
		clock.showHour = clock.AlarmHour;
		clock.showMin = clock.AlarmMin;
		clock.showSec = clock.AlarmSec;
		
	}
	/**add minutes of Alarm*/
	public void performPlus(){
		clock.AlarmMin++;
		if(clock.AlarmMin>59){
			clock.AlarmMin = 0;
		}
	}
	/**minus minutes of Alarm*/
	public void performeMinus(){
		clock.AlarmMin--;
		if(clock.AlarmMin<0){
			clock.AlarmMin = 59;
		}
	}
}
