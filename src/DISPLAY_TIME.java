import java.util.Calendar;

import javax.sound.sampled.Clip;
/**
 * DISPLAY_TIME is STATE for showing the current time.
 * @author nattapat sukpootanan.
 * */

public class DISPLAY_TIME extends ClockState{
	/**Constuctor*/
	public DISPLAY_TIME(Clock clock){
		super(clock);
		
	}
	/**updating time*/
	public void updateTime(){
		Calendar c = Calendar.getInstance();
		clock.hours = c.get(Calendar.HOUR_OF_DAY);
		clock.minutes = c.get(Calendar.MINUTE);
		clock.seconds = c.get(Calendar.SECOND);
		clock.showHour = clock.hours;
		clock.showMin = clock.minutes;
		clock.showSec = clock.seconds;
		
		
		if(clock.AlarmHour==clock.hours&&clock.AlarmMin==clock.minutes&&
				clock.AlarmSec==clock.seconds&&clock.isAlarmOn){		
		Clock.clip.start();
		Clock.clip.loop(Clip.LOOP_CONTINUOUSLY);
		clock.isAlarmOn = false;
		clock.setState( clock.ALARM_STATE);
		}
	}
	/**set state to SET_HOUR*/
	@Override
	public void performSet() {
		clock.AlarmHour = clock.AlarmHour;
		clock.AlarmMin = clock.AlarmMin;
		clock.AlarmSec = clock.AlarmSec;
		clock.setState( clock.SET_HOUR);
	}
	/**set state to DISPLAY_ALARM*/
	public void performPlus(){
		clock.showHour = clock.AlarmHour;
		clock.showMin = clock.AlarmMin;
		clock.showSec = clock.AlarmSec;
		clock.setState( clock.DISPLAY_ALARM);
	}


}
