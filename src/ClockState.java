/**
 * ClockState is State of Clock.
 * @author nattapat sukpootanan.
 * */
public abstract class ClockState {
protected Clock clock; // clock model or controller
protected boolean blinkHour = false;
protected boolean blinkMin = false;
protected boolean blinkSec = false;
public ClockState(Clock clock) { 
	this.clock = clock;
}
/** handle "set" key press */
public void performSet( ){} ;
/** handle updateTime event notification */
public abstract void updateTime();
/** handle "plus" key press */
public void performPlus(){};
/** handle "minus" key press */
public void performeMinus(){};
}