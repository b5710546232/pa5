
/**
 * Main class is class for running UI.
 * @author nattapat sukpootanan
 * */
public class Main {
	/**
	 * main running DigitalClockUI.
	 * @param args is not used.
	 * */
	public static void main( String [] args ) {
		Clock clock = new Clock( );
		DigitalClockUI  ui = new DigitalClockUI (clock);
		ui.run();
	}

}