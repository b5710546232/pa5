import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
/**
 * Digital Clock UI is represent UI of Clock.
 * @author nattapat sukpootanan.
 * */

public class DigitalClockUI extends JFrame implements Observer {
	private JButton setButton =  new JButton("set");
	private JButton plusButton =  new JButton("+");
	private JButton minusButton =  new JButton("-");
	private JLabel hour_Digit = new JLabel();
	private JLabel hour_TenDigit = new JLabel();
	private JLabel min_Digit = new JLabel();
	private JLabel min_TenDigit = new JLabel();
	private JLabel sec_Digit = new JLabel();
	private JLabel sec_TenDigit = new JLabel();
	private Clock clock;
	private JPanel line1;
	final private int delay = 500; // milliseconds
	private ImageIcon[] icons;
	private JLabel colonLabel = new JLabel();
	private JLabel colonLabel2 = new JLabel();
	private ImageIcon colon;
	private ImageIcon blank;
	private ImageIcon statusOFF;
	private ImageIcon statusON;
	private JLabel statusLabel =new JLabel();
	private boolean isBlink = false;
	Timer timer;
	public DigitalClockUI(Clock clock){
		super("Cheap Digital Clock");
		this.clock = clock;
		this.initComponent();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
		timer = new Timer(delay,Task);
		try {
			Clock.clip.open(Clock.audio);
		} catch (LineUnavailableException | IOException e) {
//			e.printStackTrace();
		}

	}
	/**
	 * initial Component of  UI.
	 * */
	public void initComponent(){
		Container contents = this.getContentPane();
		LayoutManager flowLayout = new FlowLayout( );
		LayoutManager boxLayout = new BoxLayout(contents, BoxLayout.PAGE_AXIS);

		initNumberImage();
		contents.setLayout(boxLayout);
		
		
		JPanel line = new JPanel();
		line.setLayout(flowLayout);
		line.add(statusLabel);
		line.setBackground(Color.black);
		contents.add(line);
		
		
		line1 = new JPanel();
		line1.setLayout(flowLayout);
		line1.setBackground(Color.black);



		line1.add(this.hour_TenDigit);
		line1.add(hour_Digit);
		line1.add(colonLabel);
		line1.add(min_TenDigit);
		line1.add(min_Digit);
		line1.add(colonLabel2);
		line1.add(this.sec_TenDigit);
		line1.add(this.sec_Digit);
		contents.add(line1);

		JPanel line2 = new JPanel();
		line2.setLayout(flowLayout);
		line2.add(setButton);
		line2.add(plusButton);
		line2.add(minusButton);
		line2.setBackground(Color.black);
		contents.add(line2);
		ActionListener plusB = new PlusButtonListener();
		ActionListener minusB = new MinusButtonListener();
		ActionListener setB = new SetButtonListener();
		plusButton.addActionListener(plusB);
		minusButton.addActionListener(minusB);
		setButton.addActionListener(setB);
	}
	/**
	 * initial & create Number Image.
	 * */
	public void initNumberImage(){
		icons = new ImageIcon[10];
		Class loader = this.getClass();
		URL digit_0 = loader.getResource("images/0.gif");
		URL digit_1 = loader.getResource("images/1.gif");
		URL digit_2 = loader.getResource("images/2.gif");
		URL digit_3 = loader.getResource("images/3.gif");
		URL digit_4 = loader.getResource("images/4.gif");
		URL digit_5 = loader.getResource("images/5.gif");
		URL digit_6 = loader.getResource("images/6.gif");
		URL digit_7 = loader.getResource("images/7.gif");
		URL digit_8 = loader.getResource("images/8.gif");
		URL digit_9 = loader.getResource("images/9.gif");
		URL colon_url = loader.getResource("images/colon.png");
		URL blank_url = loader.getResource("images/blank.png");
		URL statusOFF_url = loader.getResource("images/alarm_off.png");
		URL statusON_url = loader.getResource("images/alarm_on.png");
		icons[0] = new ImageIcon(digit_0);
		icons[1] = new ImageIcon(digit_1);
		icons[2] = new ImageIcon(digit_2);
		icons[3] = new ImageIcon(digit_3);
		icons[4] = new ImageIcon(digit_4);
		icons[5] = new ImageIcon(digit_5);
		icons[6] = new ImageIcon(digit_6);
		icons[7] = new ImageIcon(digit_7);
		icons[8] = new ImageIcon(digit_8);
		icons[9] = new ImageIcon(digit_9);
		blank = new ImageIcon(blank_url);
		colon = new ImageIcon(colon_url);
		statusOFF = new ImageIcon(statusOFF_url);
		statusON = new ImageIcon(statusON_url);
		this.statusLabel.setIcon(statusOFF);
		this.hour_TenDigit.setIcon(icons[0]);
		this.hour_Digit.setIcon(icons[0]);
		this.min_TenDigit.setIcon(icons[0]);
		this.min_Digit.setIcon(icons[0]);
		this.sec_TenDigit.setIcon(icons[0]);
		this.sec_Digit.setIcon(icons[0]);
		this.colonLabel.setIcon(colon);
		this.colonLabel2.setIcon(colon);

	}
	/**
	 * updating UI.
	 * */
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof Clock){
			Clock clock = (Clock)o;
			int hour = clock.showHour;
			int min = clock.showMin;
			int sec = clock.showSec;
			if(clock.isAlarmOn){
				this.statusLabel.setIcon(statusON);
			}
			else{
				this.statusLabel.setIcon(statusOFF);
			}
			if(clock.STATE.blinkHour==true&&isBlink){
				this.hour_TenDigit.setIcon(blank);
				this.hour_Digit.setIcon(blank);
			}else{
				this.hour_TenDigit.setIcon(icons[hour/10]);
				this.hour_Digit.setIcon(icons[hour%10]);
			}
			
			
			if(clock.STATE.blinkMin==true&&isBlink){
				this.min_TenDigit.setIcon(blank);
				this.min_Digit.setIcon(blank);
			}
			else{
				this.min_TenDigit.setIcon(icons[min/10]);
				this.min_Digit.setIcon(icons[min%10]);

			}
			
			
			if(clock.STATE.blinkSec==true&&isBlink){
				this.sec_TenDigit.setIcon(blank);
				this.sec_Digit.setIcon(blank);

			}
			else{
				this.sec_TenDigit.setIcon(icons[sec/10]);
				this.sec_Digit.setIcon(icons[sec%10]);
			}
		}

	}
	/**
	 * run UI.
	 * */
	public void run(){
		timer.start();


	}
	/**
	 * for update Time every 0.5 sec.
	 * */
	ActionListener Task = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			clock.updateTime();
			isBlink=!isBlink;
			clock.addObserver( DigitalClockUI.this );
		}
	};
	/**
	 * ButtonListener of Plus Button.
	 * */
	class PlusButtonListener implements ActionListener{
		/**
		 * actionPerformed of PlusButton.
		 * */
		@Override
		public void actionPerformed(ActionEvent e) {
			clock.updateTime();
			clock.handlePlusKey();

		}		
	}
	/**
	 * ButtonListener of Minus Button.
	 * */
	class MinusButtonListener implements ActionListener{
		/**
		 * actionPerformed of MinusButton.
		 * */
		@Override
		public void actionPerformed(ActionEvent e) {
			clock.updateTime();
			clock.handleMinusKey();

		}

	}/**
	 * ButtonListener of Set Button.
	 * */
	class SetButtonListener implements ActionListener{
		/**
		 * actionPerformed of SetButton.
		 * */
		@Override
		public void actionPerformed(ActionEvent e) {
			clock.handleSetKey();

		}

	}


}

